Features:

	1.  Making rectangles
	2.  Making ellipses
	3.  Selecting shapes denoted by highlighting
	4.  Removing shapes
	5.  Moving rectangles and ellipses
	6.  Move to back and move to front buttons
	7.  Changing the background color
	8.  Using the fill and outline colors and outline thickness for new shapes
	9.  Selecting shapes should update the fill and outline colors and outline thickness control settings
	10. Changing the fill and outline colors and outline thickness for existing shapes
	11. Saving drawings to and loading drawings from JSON files
	12. Exporting drawings to image files (Pose.png)
	13. Fool proof design
	14. Attractive, bug-free application

Data:
	ArrayList<Shape> shapes
		x, y, width, height, fillColor, outlineColor, thickness
	String backgroundColor