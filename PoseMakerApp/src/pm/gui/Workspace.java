package pm.gui;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import saf.ui.AppMessageDialogSingleton;
import saf.ui.AppYesNoCancelDialogSingleton;
import pm.data.DataManager;
import static pm.PropertyType.*;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author Nathan Wood
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {
    // CONSTANTS
    final static String IMAGES_FILEPATH = "./images/";
    
    final static int BUTTON_SPACING = 8;
    final static int VBOX_SPACING = 16;
    final static int LEFTPANE_WIDTH = 256;
    final static int LEFTPANE_HEIGHT = 70;
    final static int WINDOW_WIDTH = 1600;
    final static int WINDOW_HEIGHT = 900;
    
    final static int CANVAS_WIDTH = WINDOW_WIDTH - LEFTPANE_WIDTH;
    final static int CANVAS_HEIGHT = WINDOW_HEIGHT - 100;
    
    final static int COLORPICKER_WIDTH = 128;
    final static int COLORPICKER_HEIGHT = 32;
    
    final static int THICKNESS_SLIDER_MAX = 10;
    
    final static int FONT_SIZE = 12;
    
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    
    // THIS CONTAINS ALL THE DATA AND LOGIC FOR THE POSEMAKER
    DataManager dataManager;
    
    // WE'LL PUT THE WORKSPACE INSIDE A SPLIT PANE
    SplitPane workspaceSplitPane;

    // THESE ARE THE BUTTONS FOR ADDING AND REMOVING COMPONENTS
    BorderPane leftPane;
    
    Pane rightPane;
    Canvas canvas;
    ArrayList<Button> buttons;

    // THESE WILL CONTAIN ALL OF THE CONTROLS
    VBox leftPaneVBox;
    HBox toolsHBox;
    HBox depthsHBox;
    
    // COLOR PICKERS
    ColorPicker backgroundColor, fillColor, outlineColor;
    Slider thicknessSlider;
    
    // TEXT
    Text backgroundText, fillText, outlineText, thicknessText;

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
        gui.getPrimaryScene().getWindow().setWidth(WINDOW_WIDTH);
        gui.getPrimaryScene().getWindow().setHeight(WINDOW_HEIGHT);
        
        // BUILD THE SHAPESCANVAS, TAKES CARE OF ALL EVENTS
	dataManager = (DataManager) app.getDataComponent();
        
        // WE'LL ORGANIZE OUR WORKSPACE COMPONENTS USING A BORDER PANE
	workspace = new BorderPane();

	// FIRST THE LEFT HALF OF THE SPLIT PANE
	leftPane = new BorderPane();
        leftPane.setMaxWidth(LEFTPANE_WIDTH);
        leftPane.setMinWidth(LEFTPANE_WIDTH);
        leftPane.setMaxHeight(CANVAS_HEIGHT);
        leftPane.setMinHeight(CANVAS_HEIGHT);
        
        rightPane = new Pane();
        
        // CREATE THE PANES AND BOXES FOR THE TOOLS ON THE LEFT SIDE
        leftPaneVBox = new VBox(VBOX_SPACING);
        leftPaneVBox.setMinWidth(LEFTPANE_WIDTH);
        leftPaneVBox.setMaxWidth(LEFTPANE_WIDTH);
        leftPaneVBox.setMaxHeight(CANVAS_HEIGHT);
        leftPaneVBox.setMinHeight(CANVAS_HEIGHT);
        
        toolsHBox = new HBox(BUTTON_SPACING);
        toolsHBox.setMinWidth(LEFTPANE_WIDTH);
        toolsHBox.setAlignment(Pos.CENTER_LEFT);
        
        depthsHBox = new HBox(BUTTON_SPACING);
        depthsHBox.setMinWidth(LEFTPANE_WIDTH);
        depthsHBox.setAlignment(Pos.CENTER_LEFT);

	// THIS IS THE TOP TOOLBAR
	buttons = new ArrayList();
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // MAKE THE TOOL BUTTONS
        
        // Selection
        buttons.add(initButton(props.getProperty(SELECTION_TOOL_ICON), props.getProperty(SELECTION_TOOL_TOOLTIP), true));

        // Remove
        buttons.add(initButton(props.getProperty(REMOVE_ICON), props.getProperty(REMOVE_TOOLTIP), true));
        
        // Rectangles
        buttons.add(initButton(props.getProperty(RECTANGLE_ICON), props.getProperty(RECTANGLE_TOOLTIP), false));
        
        // Ellipses
        buttons.add(initButton(props.getProperty(ELLIPSE_ICON), props.getProperty(ELLIPSE_TOOLTIP), false));
        
        // Add buttons to the toolsHBox
        toolsHBox.getChildren().addAll(buttons.get(0), buttons.get(1), buttons.get(2), buttons.get(3));
        
        // Construct the depths buttons
        buttons.add(initButton(props.getProperty(DEPTH_DOWN_ICON), props.getProperty(DEPTH_DOWN_TOOLTIP), true));
        buttons.add(initButton(props.getProperty(DEPTH_UP_ICON), props.getProperty(DEPTH_UP_TOOLTIP), true));
        
        // Add the depths buttons to the depthsHBox
        depthsHBox.getChildren().addAll(buttons.get(4), buttons.get(5));
        
        // Add all the scenes to leftPane
        leftPaneVBox.getChildren().addAll(toolsHBox, depthsHBox);
        
        // Construct the text labels and color pickers
        backgroundText = formatText("Background Color");
        backgroundColor = formatColorPicker("#ffe6b3", false);
        backgroundColor.setOnAction(e -> {
            dataManager.setBackgroundColor(colorToRGB(backgroundColor.getValue()));
        });
        
        fillText = formatText("Fill Color");
        fillColor = formatColorPicker("#ff6666", false);
        fillColor.setOnAction(e -> {
            dataManager.setFillColor(colorToRGB(fillColor.getValue()));
        });
        
        outlineText = formatText("Outline Color");
        outlineColor = formatColorPicker("#99cc99", false);
        outlineColor.setOnAction(e -> {
            dataManager.setOutlineColor(colorToRGB(outlineColor.getValue()));
        });
        
        thicknessText = formatText("Outline Thickness");
        thicknessSlider = new Slider(0.01, THICKNESS_SLIDER_MAX, THICKNESS_SLIDER_MAX / 2);
        thicknessSlider.setMinWidth(LEFTPANE_WIDTH - 40);
        thicknessSlider.setMaxWidth(LEFTPANE_WIDTH - 40);
        
        buttons.add(initButton(props.getProperty(SNAPSHOT_ICON), props.getProperty(SNAPSHOT_TOOLTIP), false));
        
        // HERE IS WHERE ALL THE BUTTON EVENT HANDLERS ARE ASSIGNED
        buttons.get(0).setOnAction(e -> {
            dataManager.selectShape();
            workspace.setCursor(Cursor.DEFAULT);
        });
        
        buttons.get(1).setOnAction(e -> {
            dataManager.removeShape();
            buttons.get(0).setDisable(dataManager.canvasIsEmpty());
            buttons.get(1).setDisable(true);
            buttons.get(4).setDisable(true);
            buttons.get(5).setDisable(true);
        });
        
        buttons.get(2).setOnAction(e -> {
            dataManager.createRectangle();
            workspace.setCursor(Cursor.CROSSHAIR);
        });
        
        buttons.get(3).setOnAction(e -> {
            dataManager.createEllipse();
            workspace.setCursor(Cursor.CROSSHAIR);
        });
        
        buttons.get(4).setOnAction(e -> {
            dataManager.moveDown();
        });
        
        buttons.get(5).setOnAction(e -> {
            dataManager.moveUp();
        });
        
        buttons.get(6).setOnAction(e -> {
            dataManager.takeSnapshot();
        });
        
        thicknessSlider.valueProperty().addListener(e -> {
            dataManager.setThickness(thicknessSlider.valueProperty().doubleValue());
        });
        
        // Update the state of the tools based on mouse clicks in the canvas region
        rightPane.setOnMouseClicked(e -> {
            buttons.get(0).setDisable(dataManager.canvasIsEmpty());
            buttons.get(1).setDisable(!dataManager.isShapeSelected());
            buttons.get(4).setDisable(!dataManager.isShapeSelected());
            buttons.get(5).setDisable(!dataManager.isShapeSelected());
            
            if(dataManager.isShapeSelected()) {
                fillColor.setValue((Color)dataManager.getSelectedShape().getFill());
                outlineColor.setValue((Color)dataManager.getSelectedShape().getStroke());
                thicknessSlider.setValue(dataManager.getSelectedShape().getStrokeWidth());
            }
            
            app.getGUI().updateToolbarControls(false);
        });
        
        // ADD ALL THE CONTROLS TO THE LAYOUT
        leftPaneVBox.getChildren().addAll(backgroundText, backgroundColor, fillText, fillColor,
                outlineText, outlineColor, thicknessText, thicknessSlider, buttons.get(6));
        
        leftPane.setLeft(leftPaneVBox);
        
        rightPane.getChildren().add(dataManager.getCanvas());
        
	// AND NOW PUT IT IN THE WORKSPACE
	workspaceSplitPane = new SplitPane();
	workspaceSplitPane.getItems().addAll(leftPane, rightPane);

	// AND FINALLY, LET'S MAKE THE SPLIT PANE THE WORKSPACE
	workspace = new Pane();
	workspace.getChildren().add(workspaceSplitPane);

        // NOTE THAT WE HAVE NOT PUT THE WORKSPACE INTO THE WINDOW,
	// THAT WILL BE DONE WHEN THE USER EITHER CREATES A NEW
	// COURSE OR LOADS AN EXISTING ONE FOR EDITING
	workspaceActivated = false;
    }
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        
        backgroundColor.getStyleClass().add("button");
        fillColor.getStyleClass().add("button");
        outlineColor.getStyleClass().add("button");
        
        backgroundText.getStyleClass().add("heading_label");
        fillText.getStyleClass().add("heading_label");
        outlineText.getStyleClass().add("heading_label");
        thicknessText.getStyleClass().add("heading_label");
        
        leftPane.getStyleClass().add("bordered_pane");
    }

    /**
     * This function reloads all the controls and clears the workspace.
     */
    @Override
    public void reloadWorkspace() {        
        if(dataManager.justLoaded()) {
            buttons.get(0).setDisable(dataManager.canvasIsEmpty());
            
            backgroundColor.setValue(Color.web(dataManager.getBackgroundColor()));
            fillColor.setValue(Color.web("#ff6666"));
            outlineColor.setValue(Color.web("#99cc99"));
            thicknessSlider.setValue(THICKNESS_SLIDER_MAX / 2);
        } else {
            // Clear the canvas
            dataManager.clearCanvas();

            // Disable buttons
            buttons.get(0).setDisable(true);
            
            // Reset colors
            backgroundColor.setValue(Color.web("#ffe6b3"));
            dataManager.setBackgroundColor(colorToRGB(backgroundColor.getValue()));
            
            fillColor.setValue(Color.web("#ff6666"));
            dataManager.setFillColor(colorToRGB(fillColor.getValue()));
            
            outlineColor.setValue(Color.web("#99cc99"));
            dataManager.setOutlineColor(colorToRGB(outlineColor.getValue()));
            
            thicknessSlider.setValue(THICKNESS_SLIDER_MAX / 2);
            dataManager.setThickness(thicknessSlider.valueProperty().doubleValue());
        }
        
        buttons.get(1).setDisable(true);
        buttons.get(4).setDisable(true);
        buttons.get(5).setDisable(true);
        
        // Reset cursor
        workspace.setCursor(Cursor.DEFAULT);
    }
    
    private Button initButton(String icon, String tooltip, boolean disable) {	
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = "file:" + IMAGES_FILEPATH + icon;
        Image buttonImage = new Image(imagePath);
	
	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
	button.setAlignment(Pos.TOP_LEFT);
        button.setDisable(disable);
        
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }
    
    /**
     * Helper method to construct Text objects for the workspace
     * @param string
     *  The string for the text
     * @return 
     *  Returns a formatted Text object with the supplied string
     */
    private Text formatText(String string) {
        Text text = new Text(string);
        text.setFont(new Font(FONT_SIZE));
        return text;
    }
    
    /**
     * Helper method to construct a ColorPicker
     * @param color
     *  The hex string for the default color
     * @return 
     *  Returns a formatted ColorPicker object
     */
    private ColorPicker formatColorPicker(String color, boolean disable) {
        ColorPicker colorPicker = new ColorPicker(Color.web(color));
        
        colorPicker.setMinWidth(COLORPICKER_WIDTH);
        colorPicker.setMinHeight(COLORPICKER_HEIGHT);
        colorPicker.setDisable(disable);
        
        return colorPicker;
    }
    
     /**
     * Converts a Color object to its equivalent hex string
     * @param color
     *  The Color object to convert
     * @return 
     *  Returns a hex string
     */
    private String colorToRGB(Color color) {
        return String.format( "#%02X%02X%02X",
            (int)( color.getRed() * 255 ),
            (int)( color.getGreen() * 255 ),
            (int)( color.getBlue() * 255 ) );
    }
}
