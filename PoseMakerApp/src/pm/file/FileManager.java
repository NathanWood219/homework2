package pm.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import pm.data.DataManager;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author Nathan Wood
 * @version 1.0
 */
public class FileManager implements AppFileComponent {
    // FOR JSON SAVING AND LOADING
    final static String JSON_SHAPES_ARRAY = "shapes";
    final static String JSON_SHAPE_TYPE = "type";
    final static String JSON_SHAPE_X = "x";
    final static String JSON_SHAPE_Y = "y";
    final static String JSON_SHAPE_WIDTH = "width";
    final static String JSON_SHAPE_HEIGHT = "height";
    final static String JSON_SHAPE_FILLCOLOR = "fillColor";
    final static String JSON_SHAPE_OUTLINECOLOR = "outlineColor";
    final static String JSON_SHAPE_THICKNESS = "thickness";
    
    final static String JSON_BACKGROUNDCOLOR = "backgroundColor";

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {        
        StringWriter sw = new StringWriter();
        
        DataManager dataManager = (DataManager)data;
        
        // THEN THE ARRAYS
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
	
        ArrayList<Shape> shapes = dataManager.getShapes();
        
        for(int i = 0; i < shapes.size(); i++) {
            arrayBuilder.add(makeShapeJsonObject(shapes.get(i)));
        }
        
	JsonArray shapesArray = arrayBuilder.build();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_SHAPES_ARRAY, shapesArray)
		.add(JSON_BACKGROUNDCOLOR, dataManager.getBackgroundColor())
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    // HELPER METHOD FOR SAVING DATA TO A JSON FORMAT
    private JsonObject makeShapeJsonObject(Shape shape) {
        double x, y, width, height, thickness;
        String fillColor, outlineColor;
        
        int shapeType = -1;
        
        if(shape instanceof Rectangle) {
            shapeType = 0;
        } else if(shape instanceof Ellipse) {
            shapeType = 1;
        }
        
        if(shapeType == 0) {
            Rectangle newShape = (Rectangle)shape;
            
            x = newShape.getX();
            y = newShape.getY();
            width = newShape.getWidth();
            height = newShape.getHeight();
            fillColor = "#" + newShape.getFill().toString().substring(2, 8);
            outlineColor = "#" + newShape.getStroke().toString().substring(2, 8);
            thickness = newShape.getStrokeWidth();
        } else if(shapeType == 1) {
            Ellipse newShape = (Ellipse)shape;
            
            x = newShape.getCenterX();
            y = newShape.getCenterY();
            width = newShape.getRadiusX();
            height = newShape.getRadiusY();
            fillColor = "#" + newShape.getFill().toString().substring(2, 8);
            outlineColor = "#" + newShape.getStroke().toString().substring(2, 8);
            thickness = newShape.getStrokeWidth();
        } else {
            return null;
        }
        
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_SHAPE_TYPE, shapeType)
                .add(JSON_SHAPE_X, x)
                .add(JSON_SHAPE_Y, y)
                .add(JSON_SHAPE_WIDTH, width)
                .add(JSON_SHAPE_HEIGHT, height)
                .add(JSON_SHAPE_FILLCOLOR, fillColor)
                .add(JSON_SHAPE_OUTLINECOLOR, outlineColor)
                .add(JSON_SHAPE_THICKNESS, thickness)
                .build();
        return jso;
    }
      
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {        
        DataManager dataManager = (DataManager)data;
        
        ArrayList<Shape> shapes = new ArrayList<>();
        ArrayList<Integer> shapeTypes = new ArrayList<>();
        String backgroundColor;
        
        JsonObject jsonObject = loadJSONFile(filePath);
        
        JsonArray array = jsonObject.getJsonArray(JSON_SHAPES_ARRAY);
        
        int index = 0;
        
        try {
            while(!array.isNull(index)) {
                JsonObject obj = array.getJsonObject(index);
                int type = obj.getInt(JSON_SHAPE_TYPE);

                if(type == 0) {
                    Rectangle rect = new Rectangle();

                    rect.setX(obj.getJsonNumber(JSON_SHAPE_X).doubleValue());
                    rect.setY(obj.getJsonNumber(JSON_SHAPE_Y).doubleValue());
                    rect.setWidth(obj.getJsonNumber(JSON_SHAPE_WIDTH).doubleValue());
                    rect.setHeight(obj.getJsonNumber(JSON_SHAPE_HEIGHT).doubleValue());
                    rect.setFill(Color.web(obj.getJsonString(JSON_SHAPE_FILLCOLOR).toString().substring(1, 8)));
                    rect.setStroke(Color.web(obj.getJsonString(JSON_SHAPE_OUTLINECOLOR).toString().substring(1, 8)));
                    rect.setStrokeWidth(obj.getJsonNumber(JSON_SHAPE_THICKNESS).doubleValue());

                    shapes.add(rect);
                    shapeTypes.add(0);
                } else {
                    Ellipse ell = new Ellipse();

                    ell.setCenterX(obj.getJsonNumber(JSON_SHAPE_X).doubleValue());
                    ell.setCenterY(obj.getJsonNumber(JSON_SHAPE_Y).doubleValue());
                    ell.setRadiusX(obj.getJsonNumber(JSON_SHAPE_WIDTH).doubleValue());
                    ell.setRadiusY(obj.getJsonNumber(JSON_SHAPE_HEIGHT).doubleValue());
                    ell.setFill(Color.web(obj.getJsonString(JSON_SHAPE_FILLCOLOR).toString().substring(1, 8)));
                    ell.setStroke(Color.web(obj.getJsonString(JSON_SHAPE_OUTLINECOLOR).toString().substring(1, 8)));
                    ell.setStrokeWidth(obj.getJsonNumber(JSON_SHAPE_THICKNESS).doubleValue());

                    shapes.add(ell);
                    shapeTypes.add(1);
                }

                index++;
            }
        } catch(IndexOutOfBoundsException e) {
            // This is fine.
        }
        
        backgroundColor = jsonObject.getJsonString(JSON_BACKGROUNDCOLOR).toString().substring(1, 8);
        
        // RELOAD WITH THE NEW DATA
        dataManager.loadNewCanvas(shapes, backgroundColor);
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {

    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// NOTE THAT THE Web Page Maker APPLICATION MAKES
	// NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
	// EXPORTED WEB PAGES
    }
}
