package pm.data;

import java.io.File;
import java.io.IOException;
import saf.components.AppDataComponent;
import saf.AppTemplate;
import java.util.ArrayList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javax.imageio.ImageIO;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author Nathan Wood
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    // MEMBER VARIABLES
    Canvas canvas;
    GraphicsContext graphicsContext;
    
    boolean justLoaded = false;
    
    int width, height;
    String backgroundColor = "#ffe6b3", fillColor = "#ff6666", outlineColor = "#99cc99";
    double thickness;
    
    ArrayList<Shape> shapes;
    Shape selectedShape;                // reference to the currently selected shape
    Shape tempShape;                    // used for displaying a shape as its being created
    
    boolean selectShape;
    boolean movingShape = false;
    boolean createRectangle;
    boolean createEllipse;
    
    double mouseX, mouseY;
    
    // These are used to create a new shape
    double startX, startY;

    /**
     * This constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
        
        shapes = new ArrayList();
        
        width = 1344;
        height = 800;
        
        canvas = new Canvas(width, height);
        
        graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.setFill(Color.web(backgroundColor));
        graphicsContext.fillRect(0, 0, width, height);
        
        canvas.setOnMousePressed(e -> {
            mouseX = e.getX();
            mouseY = e.getY();
            mouseLeftPressed();
            updateCanvas();
        });
        
        canvas.setOnMouseReleased(e -> {
            mouseX = e.getX();
            mouseY = e.getY();
            mouseLeftReleased();
            updateCanvas();
        });
        
        canvas.setOnMouseDragged(e -> {
            mouseX = e.getX();
            mouseY = e.getY();
            mouseLeftDragged();
            updateCanvas();
        });
        
        resetControls();
    }
    
    /*                  MUTATORS              */
    
    public void setBackgroundColor(String color) {
        backgroundColor = color;
        updateCanvas();
    }
    
    public void setFillColor(String color) {
        fillColor = color;
        
        if(selectedShape != null) {
            selectedShape.setFill(Color.web(color));
        }
        
        updateCanvas();
    }
    
    public void setOutlineColor(String color) {
        outlineColor = color;
        
        if(selectedShape != null) {
            selectedShape.setStroke(Color.web(color));
        }
        
        updateCanvas();
    }
    
    public void setThickness(double thickness) {
        this.thickness = thickness;
        
        if(selectedShape != null) {
            selectedShape.setStrokeWidth(thickness);
        }
        
        updateCanvas();
    }
    
    public void loadNewCanvas(ArrayList<Shape> shapes, String backgroundColor) {
        this.shapes = shapes;
        this.backgroundColor = backgroundColor;
        
        justLoaded = true;
        
        updateCanvas();
    }
    
    /*                  ACCESSORS             */
    
    public Canvas getCanvas() {
        return canvas;
    }
    
    public String getBackgroundColor() {
        return backgroundColor;
    }
    
    public String getFillColor() {
        return fillColor;
    }
    
    public String getOutlineColor() {
        return outlineColor;
    }
    
    public boolean isShapeSelected() {
        return (selectedShape != null);
    }
    
    public Shape getSelectedShape() {
        return selectedShape;
    }
    
    public ArrayList<Shape> getShapes() {
        return shapes;
    }
    
    public boolean canvasIsEmpty() {
        return shapes.isEmpty();
    }
    
    /*                  METHODS               */
    
    public boolean justLoaded() {
        if(justLoaded) {
            justLoaded = false;
            return true;
        } else {
            return false;
        }
    }
    
    public void updateCanvas() {
        graphicsContext.setFill(Color.web(backgroundColor));
        graphicsContext.fillRect(0, 0, width, height);
        
        for(int i = 0; i < shapes.size(); i++) {
            if(shapes.get(i) instanceof Rectangle) {
                drawRectangle((Rectangle)shapes.get(i));
            } else if(shapes.get(i) instanceof Ellipse) {
                drawEllipse((Ellipse)shapes.get(i));
            }
        }
        
        if(tempShape != null) {
            if(tempShape instanceof Rectangle) {
                drawRectangle((Rectangle)tempShape);
            } else if(tempShape instanceof Ellipse) {
                drawEllipse((Ellipse)tempShape);
            }
        }
    }
    
    private void drawRectangle(Rectangle rect) {
        graphicsContext.setFill(rect.getFill());
        graphicsContext.fillRect(rect.getX(), rect.getY(),
                rect.getWidth(), rect.getHeight());
        graphicsContext.beginPath();
        
        if(selectedShape != null && selectedShape instanceof Rectangle && ((Rectangle)selectedShape).equals(rect)) {
            graphicsContext.setStroke(Color.web("#FFFC57"));
        } else {
            graphicsContext.setStroke(rect.getStroke());
        }
        
        graphicsContext.setLineWidth(rect.getStrokeWidth());
        graphicsContext.rect(rect.getX(), rect.getY(),
                rect.getWidth(), rect.getHeight());
        graphicsContext.stroke();
    }
    
    private void drawEllipse(Ellipse ell) {
        graphicsContext.setFill(ell.getFill());
        graphicsContext.fillOval(ell.getCenterX() - ell.getRadiusX(),
                ell.getCenterY() - ell.getRadiusY(),
                ell.getRadiusX() * 2, ell.getRadiusY() * 2);
        graphicsContext.beginPath();

        if(selectedShape != null && selectedShape instanceof Ellipse && ((Ellipse)selectedShape).equals(ell)) {
            graphicsContext.setStroke(Color.web("#FFFC57"));
        } else {
            graphicsContext.setStroke(ell.getStroke());
        }
        
        graphicsContext.setLineWidth(ell.getStrokeWidth());
        graphicsContext.strokeOval(ell.getCenterX() - ell.getRadiusX(),
                ell.getCenterY() - ell.getRadiusY(),
                ell.getRadiusX() * 2, ell.getRadiusY() * 2);
        graphicsContext.stroke();
    }
    
    public void resetControls() {
        selectShape = false;
        createRectangle = false;
        createEllipse = false;
    }
    
    public void clearCanvas() {
        shapes.clear(); 
        deselectShape();
        resetControls();
        updateCanvas();
    }
    
    /*                  CONTROLS               */
    
    // selectShape
    public void selectShape() {
        resetControls();
        selectShape = true;
    }
    
    public void deselectShape() {
        selectedShape = null;
    }
    
    // removeShape
    public void removeShape() {
        if(selectedShape != null) {
            shapes.remove(selectedShape);
            deselectShape();
            updateCanvas();
        }
    }
    
    // createRectangle
    public void createRectangle() {
        resetControls();
        createRectangle = true;
    }
    
    // createEllipse
    public void createEllipse() {
        resetControls();
        createEllipse = true;
    }
    
    // moveDown
    public void moveDown() {
        if(selectedShape != null) {
            int index = shapes.indexOf(selectedShape);
            
            if(index > 0) {
                swap(index, index - 1);
                updateCanvas();
            }
        }
    }
    
    // moveUp
    public void moveUp() {
        if(selectedShape != null) {
            int index = shapes.indexOf(selectedShape);
            
            if(index != -1 && index != shapes.size() - 1) {
                swap(index, index + 1);
                updateCanvas();
            }
        }
    }
    
    // Helper method for moving up and down
    private void swap(int index1, int index2) {
        Shape temp = shapes.get(index1);
        shapes.set(index1, shapes.get(index2));
        shapes.set(index2, temp);
    }
    
    // takeSnapshot
    public void takeSnapshot() {
        WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
        
        // PROMPT THE USER FOR A FILE NAME
        try {
            FileChooser fc = new FileChooser();
            fc.setInitialDirectory(new File("./work/"));
            fc.setTitle("Save as PNG");
            fc.getExtensionFilters().add(new ExtensionFilter("PNG (.png)", ".png"));

            File selectedFile = fc.showSaveDialog(app.getGUI().getWindow());
            if (selectedFile != null) {
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", selectedFile);
            }
        } catch(IOException e) {
            System.out.println("Error saving snapshot.");
        }
    }
    
    // MOUSE EVENTS
    
    // mouseLeftPressed
    public void mouseLeftPressed() {
        if(createEllipse || createRectangle) {
            startX = mouseX;
            startY = mouseY;
        } else if(selectShape) {
            boolean foundShape = false;
            
            for(int i = shapes.size() - 1; i >= 0; i--) {
                if(shapes.get(i) instanceof Rectangle) {
                    Rectangle rect = (Rectangle)shapes.get(i);
                    
                    if(rect.getX() < mouseX && rect.getX() + rect.getWidth() > mouseX
                            && rect.getY() < mouseY && rect.getY() + rect.getHeight() > mouseY) {
                        
                        startX = mouseX - rect.getX();
                        startY = mouseY - rect.getY();
                        
                        selectedShape = rect;
                        foundShape = true;
                        break;
                    }
                } else if(shapes.get(i) instanceof Ellipse) {
                    Ellipse ell = (Ellipse)shapes.get(i);
                    
                    if(ell.getCenterX() - ell.getRadiusX() < mouseX
                            && ell.getCenterX() + ell.getRadiusX() > mouseX
                            && ell.getCenterY() - ell.getRadiusY() < mouseY
                            && ell.getCenterY() + ell.getRadiusY() > mouseY) {
                        
                        startX = mouseX - ell.getCenterX();
                        startY = mouseY - ell.getCenterY();
                        
                        selectedShape = ell;
                        foundShape = true;
                        break;
                    }
                }
            }
            
            if(!foundShape) {
                selectedShape = null;
            } else {
                movingShape = true;
            }
        }
    }
    
    // mouseLeftReleased
    public void mouseLeftReleased() {
        if(createRectangle || createEllipse) {
            shapes.add(tempShape);
            selectedShape = tempShape;
            tempShape = null;
        } else if(movingShape) {
            movingShape = false;
        }
    }
    
    // mouseLeftDragged
    public void mouseLeftDragged() {        
        if(createRectangle) {
            Rectangle rect = new Rectangle(startX, startY,
                    mouseX - startX, mouseY - startY);
            rect.setFill(Color.web(fillColor));
            rect.setStroke(Color.web(outlineColor));
            rect.setStrokeWidth(thickness);
            
            tempShape = rect;
        } else if(createEllipse) {
            Ellipse ell = new Ellipse(startX + ((mouseX - startX) / 2),
                    startY + ((mouseY - startY) / 2), (mouseX - startX) / 2,
                    (mouseY - startY) / 2);
            ell.setFill(Color.web(fillColor));
            ell.setStroke(Color.web(outlineColor));
            ell.setStrokeWidth(thickness);
            
            tempShape = ell;
        } else if(selectShape && selectedShape != null && movingShape) {
            if(selectedShape instanceof Rectangle) {
                int index = shapes.indexOf(selectedShape);
                Rectangle rect = (Rectangle)selectedShape;
                
                rect.setX(mouseX - startX);
                rect.setY(mouseY - startY);
                
                shapes.set(index, rect);
                
            } else if(selectedShape instanceof Ellipse) {
                int index = shapes.indexOf(selectedShape);
                Ellipse ell = (Ellipse)selectedShape;
                
                ell.setCenterX(mouseX - startX);
                ell.setCenterY(mouseY - startY);
                
                shapes.set(index, ell);
            }
            
        }
    }

    /**
     * This function clears out the data
     */
    @Override
    public void reset() {
        // not used
    }
}
